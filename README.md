# node-checks

[![pipeline status](https://gitlab.com/keithwoelke/node-checks/badges/master/pipeline.svg)](https://gitlab.com/keithwoelke/node-checks/commits/master)

node-checks contains various linters and test tools. Specifically, the following tools are installed:

* git
* gitlab-ci-lint
* eclint
* yamllint
* jsonlint
* eslint
* standard
* semistandard
