#!/usr/bin/env sh

[ -x "$(command -v docker)" ] || { echo "Docker is not installed."; exit 1; }

SHELLCHECK="koalaman/shellcheck-alpine:v0.8.0"

project_dir="$(cd "${0%/*}/../.." && pwd -P)"
DOCKER_PROJECT_DIR="/mnt"

echo "Running shellcheck"
docker run --rm --mount src="${project_dir}",dst="${DOCKER_PROJECT_DIR}",type=bind -w "${DOCKER_PROJECT_DIR}" --entrypoint= "${SHELLCHECK}" sh -c '
  find . \! \( -path "*/vendor*" \) \( -name "*\\.sh" -o -path "*/scripts/githooks/*" \) -print0 | xargs -0 shellcheck'
