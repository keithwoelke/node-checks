#!/usr/bin/env sh

[ -x "$(command -v docker)" ] || { echo "Docker is not installed." ; exit 1; }

: "${DOCKER_IMAGE_NAME?"DOCKER_IMAGE_NAME must be set."}"

docker push "${DOCKER_IMAGE_NAME}"
