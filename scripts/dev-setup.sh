#!/usr/bin/env bash

[ -x "$(command -v docker)" ] || { echo "Please install docker to proceed with setup." ; exit 1; }

git config core.hooksPath scripts/githooks
