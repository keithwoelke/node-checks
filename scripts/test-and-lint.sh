#!/usr/bin/env sh

set -e

script_dir="$(cd "${0%/*}" && pwd -P)"

"${script_dir}/test-and-lint/hadolint.sh"
"${script_dir}/test-and-lint/shellcheck.sh"
#"${script_dir}/test-and-lint/gitlab-ci-lint.sh"
"${script_dir}/build-docker-image.sh"
