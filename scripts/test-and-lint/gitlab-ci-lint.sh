#!/usr/bin/env sh

[ -x "$(command -v docker)" ] || { echo "Docker is not installed."; exit 1; }

NODE_CHECKS="registry.gitlab.com/keithwoelke/node-checks:523261131"

project_dir="$(cd "${0%/*}/../.." && pwd -P)"
DOCKER_PROJECT_DIR="/mnt"

echo "Running gitlab-ci-lint"
docker run --rm --mount src="${project_dir}",dst="${DOCKER_PROJECT_DIR}",type=bind -w "${DOCKER_PROJECT_DIR}" "${NODE_CHECKS}" gitlab-ci-lint build/ci/gitlab-ci.yaml
